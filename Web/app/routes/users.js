let router = require('express').Router()
let ctrl = require('../controllers/user')

router.get('/', ctrl.getAll)
router.post('/add/:id?', ctrl.storeUser)
router.get('/add/:id?', ctrl.addUser)
router.get('/detail/:id', ctrl.detailUser)
router.get('/del/:id', ctrl.delUser)
router.get('/changePass/:id', ctrl.changePassV)
router.post('/changePass/:id', ctrl.changePass)

module.exports = router;
