let router = require('express').Router()
let ctrl = require('../controllers/auth')


router.get('/', ctrl.auth)
router.post('/', ctrl.authServ)
router.post('/test', ctrl.test)

module.exports = router;
