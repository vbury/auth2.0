var http = require('http');
var request = require('request');

var options = "http://localhost:8080/api/v1/user/"

module.exports.getAll = (req, res) => {
    request.get(options, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            let users = JSON.parse(body);
            console.log(users);
            res.render('listuser', {
                users: users,
                title: 'Liste des utilisateurs'
            });
        }
    });
}
module.exports.detailUser = (req, res) => {
    console.log(options + req.params.id);
    request.get(options + req.params.id, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            let users = JSON.parse(body);
            console.log(users);
            let title = users.lastname
            title += users.firstname
            console.log(title);
            res.render('detail', {
                users: users,
                title: title
            });
        }
    });
}
module.exports.addUser = (req, res) => {
    if (req.params.id) {
        request.get(options + req.params.id, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                let user = JSON.parse(body);
                console.log(user);
                res.render('add', {
                    user: user,
                    title: "Modification utilisateur",
                    error: req.error
                });
            }
        });
    } else {
        res.render('add', {
            title: 'Ajouter un utilisateur',
            error: req.error
        });
    }
}

module.exports.storeUser = (req, res) => {
    if (req.params.id) {
        req.body.address = {
            street: req.body.street,
            number: req.body.number,
            town: req.body.town,
            postalCode: req.body.postalCode,
            country: req.body.country
        }
        delete req.body.street
        delete req.body.number
        delete req.body.town
        delete req.body.postalCode
        delete req.body.country
        delete req.body.submit
        request.put(options + req.params.id, {
            json: req.body
        }, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                console.log(body);
               if (body.error) {
                    res.render('add', {
                        user: req.body,
                        error: body.error
                    })
                } 
                if(body.message){
                    res.redirect('/users');
                }
            }
        });
    } else {
        req.body.birthdate = req.body.birthdate + "T00:00:00Z";
        req.body.address = {
            street: req.body.street,
            number: req.body.number,
            town: req.body.town,
            postalCode: req.body.postalCode,
            country: req.body.country
        }
        delete req.body.street
        delete req.body.number
        delete req.body.town
        delete req.body.postalCode
        delete req.body.country
        delete req.body.submit
        request.post(options, {
            json: req.body
        }, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                console.log(body);
                if (body.error) {
                    res.render('add', {
                        user: req.body,
                        error: body.error
                    })
                } 
                if(body.message){
                    res.redirect('/users');
                }
            }
        });
    }
}

module.exports.delUser = (req, res) => {
    request.delete(options + req.params.id, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            console.log(body);
        }
        res.redirect('/users')
    });
}
module.exports.changePassV = (req, res) => {
    res.render('changePass',{
        id : req.params.id
    })
}

module.exports.changePass = (req, res) => {
    if(req.body.newPassword=req.body.newPassword2){
        delete req.body.newPassword2
        request.post(options + "changePass/" + req.params.id, {
            json: req.body
        }, function(error, response, body) {
            if (!error && response.statusCode == 200) {
                console.log(body);
                res.redirect("/users")
            }
            if(body.error){
                console.log(body.error);
                res.render('changePass' , {
                        id: req.params.id,
                        error: body.error
                    })
            }
        });
    }
}