var request = require('request');

var options = "http://localhost:8080/api/v1/auth/"

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

module.exports.auth = (req, res) => {
    res.render('auth')
}

module.exports.authServ = (req, res) => {
    let email = validateEmail(req.body.usernameoremail)
    if (email) {
        req.body.email = req.body.usernameoremail
        req.body.username ="";
        delete req.body.usernameoremail
        console.log(req.body);
        request.get(options, {
            json: req.body
        }, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                console.log(body);
                if (body.sucess == false) {
                    res.render('auth', {
                        error: body.message
                    })
                }
            }
        });
    } else {
        req.body.username = req.body.usernameoremail
        req.body.email = "";
        delete req.body.usernameoremail
        console.log(req.body);
        request.get(options, {
            json: req.body
        }, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                console.log(body);
                if (body.sucess == false) {
                    res.render('auth', {
                        error: body.message
                    })
                } else {
                    res.render('test',{
                        token: body.token
                    })
                }
            }
        });
    }
}

module.exports.test = (req, res) => {
    let token = req.body
    console.log(token);
    request.get(options + "verify/", {
        json: token
    }, function(error, response, body) {
        if (!error && response.statusCode == 200) {
            console.log(body);
        }
    });
}