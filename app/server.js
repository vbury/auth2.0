let express = require('express');
let app = express();
let bodyParser = require('body-parser');
let morgan = require('morgan');


let index = require('./routes/index');
let auth = require('./routes/auth');
let user = require('./routes/user');
let err404 = require('./routes/err404');


let port = process.env.PORT || 8080;

app.use(bodyParser.urlencoded({extended: true})); // for parsing application/x-www-form-urlencoded
app.use(bodyParser.json()); // for parsing application/json
app.disable('x-powered-by');
app.use((req, res, next) => {
	res.header("Access-Control-Allow-Origin", "*")
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
	next()
});

app.use(morgan('dev'));

app.use('/api/v1/', index);
app.use('/api/v1/auth', auth);
app.use('/api/v1/user', user);

app.use('*', err404);

app.listen(port);
console.log('Magic happens at http://localhost:' + port);