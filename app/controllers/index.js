let port = process.env.PORT || 8080;

module.exports.getIndex = (req, res) => {
    res.send('Hello! The API is at http://localhost:' + port + '/api/v1/');
}