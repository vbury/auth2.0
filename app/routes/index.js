let router = require('express').Router()
let ctrl = require('../controllers/index')

router.get('/', ctrl.getIndex)

module.exports = router;