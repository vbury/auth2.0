let router = require('express').Router()
let ctrl = require('../controllers/user')

router.get('/', ctrl.getAll)
router.post('/', ctrl.addUser)
router.get('/:id', ctrl.getById)
router.delete('/:id', ctrl.delUser)
router.put('/:id', ctrl.updateUser)
router.post('/changePass/:id', ctrl.changePass)

module.exports = router;
